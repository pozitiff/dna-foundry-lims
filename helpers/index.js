/**
 * Helpers
 */

/**
 * Returns random integer number
 * @param  {Number} min Minimal number
 * @param  {Number} max Maximum number
 * @return {Number}     Number
 */
function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  rand = Math.floor(rand);
  return rand;
}

/**
 * Fake Report generator
 * @return {Object} Report object
 */
function generateReport() {
  const report = {
    columns: [
      { text: 'Name', datafield: 'name', width: '15%' },
      { text: 'Sequence', datafield: 'sequence', width: '70%' },
      { text: 'Length', datafield: 'size', width: '5%', cellsalign: 'right' },
      { text: 'Organism', datafield: 'organism', width: '10%' },
    ],
    rows: [],
    header: 'Terminators'
  }
  /** Fill an array of rows */
  for (var i = 1, j = 0; i <= 40; i++) {
    if (i == 10) { j = ''; }
    const name = `Terminator${j}${i}`;
    const size = randomInteger(100, 999);
    const sequenceList = ['T', 'TT', 'C', 'CC', 'G', 'GG', 'A', 'AA'];
    let sequence = '';
    for (var k = 1; k <= randomInteger(100, 999); k++) {
      const sequenceRandom = Math.floor(Math.random() * sequenceList.length);
      sequence += sequenceList[sequenceRandom];
    }
    const organismList = ['Yeast', 'Mammal', 'Bacterial', 'Plant'];
    const organismRandom = Math.floor(Math.random() * organismList.length);
    const organism = organismList[organismRandom];

    report.rows.push({ name, sequence, size, organism });
  }
  return report;
}

/** Exports */
module.exports = {
  generateReport,
}