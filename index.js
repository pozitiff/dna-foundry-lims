/**
 * Simple endpoint which returns fake Report data and render GET report page.
 */

/** Dependencies */
const helpers = require('./helpers/index');
const express = require('express');
const path = require('path');

/** Create Express instance */
const app = express();

/** Middleware */
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/** Serve static files */
app.use('/', express.static('public'));

/** Routes */
app.get('/api', (req, res) => {
  res.header('Content-Type', 'application/json');
  const report = helpers.generateReport();
  res.send(report);
});

/** Start endpoint */
app.listen(8080, () => console.log('Server started on port 8080'));