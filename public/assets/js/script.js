/**
 * GET Report data from server and represent it as a table
 */

$(document).ready(function() {
  //** Handle click event to GET report data */
  $('#submit').submit(function(e) {
    e.preventDefault();
    var endpoint = $('#endpoint');
    if (endpoint.val() == '') {
      endpoint.addClass('uk-form-danger');
      notify('Enter the URL!');
      return;
    } else {
      endpoint.removeClass('uk-form-danger');
      getReport(endpoint.val());
    }
  });
});

/**
 * GET Report data and show it
 * @param  {Object|JSON} endpoint HTTP endpoint
 * @return {null}          null
 */
function getReport(endpoint) {
  $.get(endpoint, function(report) {
    report = objectOrJSON(report);
    var cond = !report || jQuery.isEmptyObject(report) || typeof report == 'undefined' || typeof report != 'object';
    if (cond) {
      return notify('Empty response!');
    }
    var reportData = report.rows;
    var metaData = report.columns;
    var header = report.header;
    var source = {
      localdata: reportData,
      datatype: 'json',
    };
    $('#header').html(header);
    var dataAdapter = new $.jqx.dataAdapter(source);
    $("#jqxgrid").jqxGrid({
      width: '100%',
      theme: 'energyblue',
      source: dataAdapter,
      columns: metaData,
      sortable: true,
      filterable: true,
      pageable: true,
      pagesize: 20,
      altrows: true,
    });
  }).fail(function() {
    $('#endpoint').addClass('uk-form-danger');
    notify('Bad request!');
  });
  return null;
}

/**
 * Helper for parse JSON
 * @param  {Object|String} obj Object or JSON
 * @return {Object}     Object
 */
function objectOrJSON(obj) {
  try {
    return JSON.parse(obj);
  } catch(e) {
    return obj;
  }
}

/**
 * Just throws notification
 * @param  {String} msg Notification message
 * @return {Object}     UIkit notification
 */
function notify(msg) {
  return UIkit.notification({
    message: msg || 'Something was wrong!',
    status: 'danger',
    pos: 'top-right',
    timeout: 3000,
  });
}