# Get Report data from the server and represent it as a table.

## Install 

    git clone git@bitbucket.org:pozitiff/dna-foundry-lims.git
    cd dna-foundry-lims
    npm i
    npm start

Then open *http://localhost:3000/* in your browser.
Enter the URL with the Report data and press "Get".
If entered URL isn't valid — you'll see a notification with the error.

API endpoint with fake Report data is located at *http://localhost:3000/api*

## Deployment via Nanobox

    nanobox remote add dna-foundry-lims
    nanobox deploy

### (optional) Test deployment

    nanobox deploy dry-run

#### Technologies list:
- HTML5
- CSS3 
- JavaScript(EcmaScript 5 — frontend EcmaScript 6 — backend)
- JQuery
- JQWidgets
- NodeJS
- ExpressJS
- UIKit
- ie9.js
- Nanobox
- Git